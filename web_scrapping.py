import requests
from bs4 import BeautifulSoup
from time import sleep
import xlsxwriter
import random
import logging

name = input("Enter a city: ")

End_page = 600
file = "zomatores"


workbook = xlsxwriter.Workbook(file+".xlsx")
worksheet = workbook.add_worksheet()

row = 0
col = 0

for i in range (1,End_page + 1):
    url = "https://www.zomato.com/" + name + "/restaurants?page="+str(i)
    headers = {'user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}

    response = requests.get(url,headers=headers)
    response.status_code
    response.headers
    response.content

    soup = BeautifulSoup(response.content,'html.parser')
    soup.prettify()
    restaurants = soup.find_all(class_='search-snippet-card')
    logging.warning('started')
    for s_res in restaurants:
        res_name = s_res.find(class_='result-title').get('title')
        res_address = s_res.find(class_='search-result-address').get('title')
        res_phone_no = s_res.find(class_='res-snippet-ph-info').get('data-phone-no-str')
        worksheet.write(row, col,     res_name)
        worksheet.write(row, col + 1, res_address)
        worksheet.write(row, col + 2, res_phone_no)
        row += 1
    logging.warning(row)
    timeDelay = random.randint(0,5)
    print ("delay: %d." % timeDelay)
    sleep(timeDelay)

    

workbook.close()